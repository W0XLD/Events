# Why Amateur Radio? / How to Get Started

## Why Amateur Radio?

Amateur radio is:
* A fun hobby. Build your own radio station, and talk to people around the world!
* Emergency preparedness. When infrastructure fails, you can still get help, and serve your community!
* Keeping in touch. Even when your adventures take you past the boundaries of mobile phone coverage!
* Making new friends. "Hams" love to share knowledge, experience, and good times with each other!

## How to Get Started

You will need to pass a knowledge test to be licensed by the Federal Communications Commission (FCC). The test covers laws and regulations, theory and technical knowledge, safety, and operating skills.

* Study for the test
    * Books:
        * *HamRadioSchool.com Technician License Course* by Stu Turner
            * https://hamradioschool.com
        * *ARRL Ham Radio License Manual* by the American Radio Relay League
            * http://www.arrl.org/shop/Ham-Radio-License-Manual/
    * Websites:
        * HamStudy by SignalStuff
            * https://hamstudy.org
        * HamExam.org Amateur Radio Practice Exams
            * https://hamexam.org
    * Videos:
        * Ham Radio Crash Course
            * https://www.youtube.com/user/hoshnasi
        * Gary, W4EEY
            * https://www.youtube.com/c/W4EEY

* Take the test
    * Register for an FCC Registration Number (FRN):
        * https://www.fcc.gov/wireless/support/universal-licensing-system-uls-resources/getting-fcc-registration-number-frn
    * Find the date, time, and location of a test session:
        * PPRAA hosts a test session at 10:00 AM on the second Saturday of each month
        * Location: Pikes Peak Regional Office of Emergency Management, 3755 Mark Dabling Blvd, Colorado Springs CO 80907
        * Confirm date/time/location, and learn what to bring, at <http://ppraa.org/ve-testing>

* Get on the air
    * Learn how frequencies are allocated for use by the Colorado Council of Amateur Radio Clubs: <https://www.ccarc.net/frequency-use-plans/>
    * Explore repeaters that provide coverage in your area: <https://repeaterbook.com/repeaters/index.php?state_id=08>
    * Scan popular simplex frequencies for operators who are working the bands with no repeaters:
        * National 2 m FM calling frequency: 146.520 MHz
        * National 2 m FM adventure frequency: 146.580 MHz
        * Pikes Peak region chat frequency: 146.460 MHz
        * National 70 cm FM calling frequency: 446.000 MHz 

* Join the PPRAA
    * The Pikes Peak Radio Amateur Association provides events, education, and service opportunities throughout the Pikes Peak region. Make the most of your new amateur radio license with us!

# Pikes Peak Radio Amateur Association (PPRAA)

Joining your local amateur radio club is beneficial for you, and provides opportunities for you to give back to the community as well!

* On-air nets and technical discussions
    * Learning, mentorship, experience
* Monthly meetings
    * In-person presentations, door prizes, and good times with fellow hams
* Pikes Peak Radio Experience (PPREX)
    * Quarterly hands-on events give us a chance to exercise our stations, learn new skills, build new antennas, make new friends
* Pikes Peak Megafest!
    * The Pikes Peak region's most popular hamfest, held once a year. Swap meet, vendor displays, raffles, license testing, food, and fun
* Amateur radio license tests
    * Earn your FCC license to transmit. PPRAA's Volunteer Examiner team does not charge a fee for testing! Sessions are held once a month.
* Club station
    * Work the bands from an RF-quiet location, with several high-frequency antennas, radios, and digital interfaces to choose from
* Community service opportunities with Pikes Peak Amateur Radio Emergency Service (PPARES)
    * PPRAA supports PPARES and the Skywarn program, to provide communication and observation support to local served agencies and the National Weather Service
* Volunteer opportunities
    * All activities and events of the PPRAA rely on volunteers from our community. We are a 501(c)(3) nonprofit organization, and a member club of the ARRL. Your contributions and enthusiastic participation make our club and community stronger for the future.

Join today! http://ppraa.org