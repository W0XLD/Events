# Festival Resources

Handouts and activity documentation for booths or display tents at fairs, festivals, PPREX events, hamfests, etc.

## Calendar
Upcoming events:

* 2021-07-10: Western Museum of Mining & Industry: science-themed "Super Saturday" family day
    * See [WMMI events calendar for July](https://wmmi.org/news-events/events-wmmi.html/calendar/2021/7)
* 2021-07-31: Colorado Springs sesquicentennial festival downtown
* 2021-08-??: Mobile amateur radio station car show

## Handouts

* Why Amateur Radio? / How to Get Started
* Pikes Peak Radio Amateur Association (PPRAA)
